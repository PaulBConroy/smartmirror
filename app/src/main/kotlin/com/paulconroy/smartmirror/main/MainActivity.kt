package com.paulconroy.smartmirror.main

import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.widget.ImageView
import android.widget.TextView

import com.paulconroy.smartmirror.R
import com.paulconroy.smartmirror.base.BaseActivity


class MainActivity : BaseActivity<MainPresenter>(), MainView {

    var presenter : MainPresenter? = null
    private lateinit var ivWeatherIcon : ImageView
    private lateinit var tvTemp : TextView
    private lateinit var tvMessage : TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        tvTemp = findViewById(R.id.tvTemp) as TextView
        ivWeatherIcon = findViewById(R.id.ivWeatherIcon) as ImageView
        tvMessage = findViewById(R.id.tvMessage) as TextView

        getPresenter().update()

    }

    override fun getPresenter(): MainPresenterImp {
        if (presenter == null) {
            presenter = MainPresenterImp(this, this)
        }
        return presenter as MainPresenterImp
    }

    override fun updateWeatherUI(temp : String, weather: Int) {
        tvTemp?.text = temp
        ivWeatherIcon?.setImageDrawable(ContextCompat.getDrawable(this, weather))
    }

    override fun updateMessage(message : String) {
        tvMessage?.text = message
    }

}
