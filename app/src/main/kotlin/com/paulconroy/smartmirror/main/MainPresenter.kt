package com.paulconroy.smartmirror.main

/**
 * Created by paulconroy on 22/06/2017.
 */

interface MainPresenter {
    fun update()
}