package com.paulconroy.smartmirror.main

/**
 * Created by paulconroy on 22/06/2017.
 */

interface MainView {

    /**
     *Updates the weather icon and the temp UI
     */
    fun updateWeatherUI(temp : String, weather: Int)

    /**
     * updates the time of day message to the user
     */
    fun updateMessage(message : String)
}