package com.paulconroy.smartmirror.main

import android.content.Context
import android.os.Handler
import android.util.Log
import android.widget.Switch
import com.paulconroy.smartmirror.BuildConfig
import com.paulconroy.smartmirror.R
import github.vatsal.easyweather.Helper.TempUnitConverter
import github.vatsal.easyweather.Helper.WeatherCallback
import github.vatsal.easyweather.WeatherMap
import github.vatsal.easyweather.retrofit.models.WeatherResponseModel
import java.util.*

/**
 * Created by paulconroy on 22/06/2017.
 */

class MainPresenterImp(mainActivity: MainActivity, activityContext: Context) : MainPresenter {

    private var view: MainView = mainActivity
    private var context: Context = activityContext
    private var calendar : Calendar = Calendar.getInstance()

    override fun update() {
        val delay:Int = 5000
        Handler().postDelayed({
            val weatherMap = WeatherMap(context, BuildConfig.OWM_API_KEY)
            weatherMap.getCityWeather("Bray,IE", object : WeatherCallback() {
                override fun success(response: WeatherResponseModel) {
                    val weather = response.weather
                    val weatherMain = weather[0].description
                    val temperature = TempUnitConverter.convertToCelsius(response.main.temp)
                    val temp = java.lang.Long.toString(Math.round(temperature!!)) + ""
                    var draw : Int = getWeatherIcon(weatherMain)
                    view.updateWeatherUI(temp, draw)
                    view.updateMessage(getMessageString())
                    update()
                }

                override fun failure(s: String) {
                    Log.e("error ", s)
                    update()
                }
            })
        }, delay.toLong())
    }

    fun getWeatherIcon(weather : String) : Int {
        return when (weather) {
            context.getString(R.string.clear_sky) -> R.drawable.sun
            context.getString(R.string.shower_rain) -> R.drawable.drop
            context.getString(R.string.few_clouds) -> R.drawable.sun
            context.getString(R.string.Clouds) -> R.drawable.cloud
            context.getString(R.string.light_rain) -> R.drawable.drop
            context.getString(R.string.fog) -> R.drawable.cloud
            context.getString(R.string.scattered_clouds) -> R.drawable.sunny
            context.getString(R.string.broken_clouds) -> R.drawable.cloud
            context.getString(R.string.thunderstorm) -> R.drawable.storm
            context.getString(R.string.snow) -> R.drawable.snowflake
            context.getString(R.string.mist) -> R.drawable.cloud
            else -> 0
        }
    }

    fun getMessageString() : String {
        val timeOfDay : Int = calendar.get(Calendar.HOUR_OF_DAY)
        var messageOfDay : String = ""

        if(timeOfDay in 0..11) {
            messageOfDay = context.getString(R.string.good_morning)
        } else if(timeOfDay in 12..15) {
            messageOfDay = context.getString(R.string.good_afternoon)
        } else if(timeOfDay in 16..20) {
            messageOfDay = context.getString(R.string.good_evening)
        } else if(timeOfDay in 21..23) {
            messageOfDay = context.getString(R.string.good_night)
        }

        return messageOfDay
    }
}